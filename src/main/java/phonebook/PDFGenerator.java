package phonebook;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import javafx.collections.ObservableList;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

public class PDFGenerator {

    public void pdfGeneration(String fileName, ObservableList<Person> data) {

        Document document = new Document();

        try {

            // create empty pdf with the filename
            PdfWriter.getInstance(document, new FileOutputStream(fileName + ".pdf"));
            document.open();

            // logo to pdf
            Image image = Image.getInstance(getClass().getResource("/logo.png"));
            image.scaleToFit(200,86);
            image.setAbsolutePosition(200f,750f);
            document.add(image);

            document.add(new Paragraph("\n\n\n\n\n"));

            // table

            float[] columnWidth = {1,3, 3, 4};
            PdfPTable table = new PdfPTable(columnWidth);
            table.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell(new Phrase("List of Contacts"));
            cell.setBackgroundColor(GrayColor.GREEN);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);

            table.getDefaultCell().setBackgroundColor(new GrayColor(0.75f));
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell("Nr.");
            table.addCell("Firstname");
            table.addCell("Lastname");
            table.addCell("E-mail");
            table.setHeaderRows(1);

            table.getDefaultCell().setBackgroundColor(GrayColor.GRAYWHITE);
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);

            for (int i = 1; i <= data.size(); i++) {
                Person actualPerson = data.get(i - 1);

                table.addCell("" + i);
                table.addCell(actualPerson.getFirstName());
                table.addCell(actualPerson.getLastName());
                table.addCell(actualPerson.getEmail());
            }

            document.add(table);

//            document.add(new Paragraph("\n\n\n\n\n\n\n\n\n\n" + text,
//                    FontFactory.getFont("fontType", BaseFont.IDENTITY_H, BaseFont.EMBEDDED)));

            // signature to pdf
            Chunk signature = new Chunk("\n\n\n generated by Phonebook application");
            Paragraph base = new Paragraph(signature);
            document.add(base);

        } catch (Exception e) {
            e.printStackTrace();
        }
        document.close();
    }
}
