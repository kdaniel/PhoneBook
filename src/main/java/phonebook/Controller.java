package phonebook;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    TableView table;
    @FXML
    TextField inputLastName;
    @FXML
    TextField inputFirstName;
    @FXML
    TextField inputEmail;
    @FXML
    Button addNewContactButton;
    @FXML
    StackPane menuPane;
    @FXML
    Pane contactPane;
    @FXML
    Pane exportPane;
    @FXML
    TextField inputExport;
    @FXML
    Button exportButton;
    @FXML
    SplitPane mainSplit;
    @FXML
    AnchorPane anchor;

    private final String MENU_CONTACTS = "Contacts";
    private final String MENU_LIST = "List";
    private final String MENU_EXPORT = "Export";
    private final String MENU_EXIT = "Exit";

    private DB db = new DB();

    private final ObservableList<Person> data = FXCollections.observableArrayList();

    @FXML
    private void addContact(ActionEvent e) {
        String email = inputEmail.getText();
        if (email.length() > 3 && email.contains("@") && email.contains(".")) {
            Person person = new Person(inputFirstName.getText(), inputLastName.getText(), email);
            data.add(person);
            db.addPerson(person);
            inputFirstName.clear();
            inputLastName.clear();
            inputEmail.clear();
        } else {
            alert("E-mail malformed");
        }

    }

    @FXML
    private void exportList(ActionEvent event) {
        String filename = inputExport.getText().trim();
        filename = filename.replaceAll("\\s","" );
        if (filename != null && !filename.equals("")) {
            PDFGenerator pdfGenerator = new PDFGenerator();
            pdfGenerator.pdfGeneration(filename, data);
        } else {
            alert("File name is missing");
        }

    }
    public void setTableData() {
        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setMinWidth(130);
        lastNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        // personbol stringkent fogunk megjeleniteni egy lastname valtozot
        lastNameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("lastName"));

        lastNameCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Person,String> event) {
                        Person actualPerson = ((Person) event.getTableView().getItems().get(event.getTablePosition().getRow()));
                        actualPerson.setLastName(event.getNewValue());
                        db.updatePerson(actualPerson);
                    }
                }
        );

        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setMinWidth(130);
        firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("firstName"));

        firstNameCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Person,String> event) {
                        Person actualPerson = ((Person) event.getTableView().getItems().get(event.getTablePosition().getRow()));
                        actualPerson.setFirstName(event.getNewValue());
                        db.updatePerson(actualPerson);
                    }
                }
        );

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(250);
        emailCol.setCellFactory(TextFieldTableCell.forTableColumn());
        emailCol.setCellValueFactory(new PropertyValueFactory<Person, String>("Email"));

        emailCol.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<Person, String>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<Person,String> event) {
                        Person actualPerson = ((Person) event.getTableView().getItems().get(event.getTablePosition().getRow()));
                        actualPerson.setEmail(event.getNewValue());
                        db.updatePerson(actualPerson);
                    }
                }
        );


        TableColumn removeColumn = new TableColumn(" Delete");
        removeColumn.setMinWidth(100);

        Callback<TableColumn<Person,String>, TableCell<Person, String>> cellFactory =
                new Callback<TableColumn<Person, String>, TableCell<Person, String>>() {
                    @Override
                    public TableCell<Person, String> call(final TableColumn<Person, String> param) {
                        final TableCell<Person, String> cell = new TableCell<Person, String>() {
                            final Button button = new Button("delete");
                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                }else {
                                    button.setOnAction( (ActionEvent event ) -> {
                                        Person person = getTableView().getItems().get(getIndex());
                                        data.remove(person);
                                        db.removeContact(person);
                                    } );
                                    setGraphic(button);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        removeColumn.setCellFactory( cellFactory);


        table.getColumns().addAll(lastNameCol,firstNameCol,emailCol, removeColumn);
        data.addAll(db.getAllPersons());

        // set data by items from data
        table.setItems(data);
    }

    private void setMenuData() {
        TreeItem<String> treeItemRoot = new TreeItem<>("Menu");
        TreeView<String> treeView = new TreeView<>(treeItemRoot);
        treeView.setShowRoot(false);

        TreeItem<String> nodeItemA = new TreeItem<>(MENU_CONTACTS);
        TreeItem<String> nodeItemB = new TreeItem<>(MENU_EXIT);
//        nodeItemA.setExpanded(true);
        Node contactNode = new ImageView(new Image(getClass().getResourceAsStream("/contact.png")));
        Node exportNode = new ImageView(new Image(getClass().getResourceAsStream("/export.png")));


        TreeItem<String> nodeItemA1 = new TreeItem<>(MENU_LIST, contactNode);
        TreeItem<String> nodeItemA2 = new TreeItem<>(MENU_EXPORT,exportNode);
        nodeItemA.getChildren().addAll(nodeItemA1,nodeItemA2);
        treeItemRoot.getChildren().addAll(nodeItemA,nodeItemB);

        menuPane.getChildren().add(treeView);


        treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                String selectedMenu = selectedItem.getValue();

                if(null != selectedMenu) {
                    switch (selectedMenu) {
                        case MENU_CONTACTS:
                            selectedItem.setExpanded(true);
                            break;
                        case MENU_LIST:
                            contactPane.setVisible(true);
                            exportPane.setVisible(false);
                            break;
                        case MENU_EXPORT:
                            contactPane.setVisible(false);
                            exportPane.setVisible(true);
                            break;
                        case MENU_EXIT:
                            System.exit(0);
                            break;
                    }
                }
            }
        });
    }

    private void alert(String text) {
        mainSplit.setDisable(true);
        mainSplit.setOpacity(0.4);

        Label label = new Label(text);
        Button alertButton = new Button("OK");
        VBox vbox = new VBox(label, alertButton);
        vbox.setAlignment(Pos.CENTER);

        alertButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mainSplit.setDisable(false);
                mainSplit.setOpacity(1);
                vbox.setVisible(false);
            }
        });
        anchor.getChildren().add(vbox);
        anchor.setTopAnchor(vbox, 300.0);
        anchor.setLeftAnchor(vbox, 300.0);
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setTableData();
        setMenuData();
    }




}
