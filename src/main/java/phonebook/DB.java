package phonebook;

import java.sql.*;
import java.util.ArrayList;

public class DB {
    final String JDBC_DRIVER = "org.h2.Driver";
    final String URL = "jdbc:h2:/home/kocsisdaniel/development/sfj/Phonebook/phonebook;create=true";
    final String USERNAME = "SA";
    final String PASSWORD = "";

    Connection connection = null;
    Statement createStatement = null;
    DatabaseMetaData dbmd = null;
    public DB() {

        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
//            System.out.println("connection works");
        } catch (SQLException e) {
            System.out.println(e);
            e.printStackTrace();
        }

        if (connection != null) {
            try {

                createStatement = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        try {
            dbmd = connection.getMetaData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ResultSet rs1 = dbmd.getTables(null,"APP", "contacts", null);
            if (!rs1.next()) {
                createStatement.execute("CREATE TABLE `CONTACTS` (\n" +
                        "  `ID` int(11) NOT NULL AUTO_INCREMENT,\n" +
                        "  `FIRSTNAME` varchar(30) NOT NULL COMMENT 'Persons first name',\n" +
                        "  `LASTNAME` varchar(30) COMMENT 'Persons last name',\n" +
                        "  `EMAIL` varchar(40) NOT NULL,\n" +
                        "  PRIMARY KEY (`ID`),\n" +
                        ");\n");
            }
        } catch (SQLException e) {
//            System.out.println("table exists");
//            e.printStackTrace();
        }

    }
    public void addPerson(Person person) {

        try {
            String sql = "INSERT INTO `contacts` (`FIRSTNAME`, `LASTNAME`, `EMAIL`) VALUES (?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,person.getFirstName());
            preparedStatement.setString(2,person.getLastName());
            preparedStatement.setString(3,person.getEmail());
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updatePerson(Person person) {

        try {
            String sql = "update contacts set firstname = ?, lastname = ?, email = ? where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,person.getFirstName());
            preparedStatement.setString(2,person.getLastName());
            preparedStatement.setString(3,person.getEmail());
            preparedStatement.setInt(4,Integer.valueOf(person.getId()));
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Person> getAllPersons() {
        String sql = "SELECT * from CONTACTS";
        // null ha hiba, ha ures akkor nem tudnank h hiba v csak ures tabla
        ArrayList<Person> persons = null;
        try {
            ResultSet rs = createStatement.executeQuery(sql);
            persons = new ArrayList<>();
            while (rs.next()){
                Integer id = rs.getInt("id");
                String firstName = rs.getString("FIRSTNAME");
                String lastName = rs.getString("LASTNAME");
                String email = rs.getString("EMAIL");
                Person person = new Person(id, firstName, lastName, email);
                persons.add(person);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
    }

    public void removeContact(Person person) {

        try {
            String sql = "delete from contacts where id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,Integer.valueOf(person.getId()));
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
